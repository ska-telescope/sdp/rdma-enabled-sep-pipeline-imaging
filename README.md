

[![Documentation Status](https://readthedocs.org/projects/rdma-enabled-sep-pipeline-imaging/badge/?version=latest)](https://developer.skatelescope.org/projects/rdma-enabled-sep-pipeline-imaging/en/latest/?badge=latest)


RDMA Data Transport integration with SEP Pipeline
=================================================

The following are a set of instructions of running the RDMA Data Transport integration with SEP Pipeline on Kubernetes, and has been tested on k8s v1.17.3 on Ubuntu 18.04.

