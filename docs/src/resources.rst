Available resources
===================

The folder called "resources" is a collection of resources used for testing and for configuration. 

Makefile targets
----------------
This project contains a Makefile which defines the following targets:

+-----------------+---------------------------------------------------------------------+
| Makefile target | Description                                                         |
+=================+=====================================================================+
| vars            | Display variables                                                   |
+-----------------+---------------------------------------------------------------------+
| k8s             | Which kubernetes are we connected to                                |
+-----------------+---------------------------------------------------------------------+
| logs            | POD logs for descriptor                                             |
+-----------------+---------------------------------------------------------------------+
| namespace       | create the kubernetes namespace                                     |
+-----------------+---------------------------------------------------------------------+
| deploy          | deploy the helm chart                                               |
+-----------------+---------------------------------------------------------------------+
| show            | show the helm chart                                                 |
+-----------------+---------------------------------------------------------------------+
| delete          | delete the helm chart release                                       |
+-----------------+---------------------------------------------------------------------+
| gangway         | install gangway authentication for gitlab (kube-system namespace)   |
+-----------------+---------------------------------------------------------------------+
| poddescribe     | describe Pods executed from Helm chart                              |
+-----------------+---------------------------------------------------------------------+
| podlogs         | show Helm chart POD logs                                            |
+-----------------+---------------------------------------------------------------------+
| help            | Show the help summary                                               |
+-----------------+---------------------------------------------------------------------+


Ansible
-------

It is possible to setup a local SKAMPI environment using the ansible playbook available `here <https://github.com/ska-telescope/ansible-playbooks#skampi>`_.


.. toctree::
   :maxdepth: 1
   :caption: Readme File:

   README
